package com.tuling.statically;

import org.junit.Test;

/***
 * @Author 徐庶   QQ:1092002729
 * @Slogan 致敬大师，致敬未来的你
 *
静态代理的局限性:
 1. 代理处理类需要自己编写
 2.不能做到任何类任何方法的代理。
 3. 只能代理某一类型接口的实例，不能代理任意接口任意方法的操作。
 */
public class MainTest  {

    @Test
    public void test(){
        ISEO proxy=new CEOProxy("徐庶");
        // 客户接见
        proxy.meeting();
    }
}
