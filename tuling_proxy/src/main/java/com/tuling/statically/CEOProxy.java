package com.tuling.statically;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/***
 * @Author 徐庶
 * @Slogan 致敬大师，致敬未来的你
 *
 *  秘书
 */
public class CEOProxy implements ISEO  {
    // 目标类
    private ISEO ceo;
    @Override
    public void meeting() {
        // 日常开发
        //  日志记录方法参数
        System.out.println("秘书登记来访："+name+ LocalDateTime.now().toString());
        //  鉴权
        if(name.equals("徐庶")) {
            // 开始时间
            Date sdate = new Date();

            // 调用目标类方法
            ceo.meeting();

            // 结束时间
            Date edate = new Date();
            //  统计执行时间（毫秒）
            // 方法的耗时
            System.out.printf("会谈时间：%d 毫秒.", (edate.getTime() - sdate.getTime()));
        }
    }


    // 无法动态代理其他方法
    //


    private String name;
    public CEOProxy(String name) {
        this.name=name;
        this.ceo=new CEO(name);
    }

}
