package com.tuling.statically;

/***
 * @Author 徐庶   QQ:1092002729
 * @Slogan 致敬大师，致敬未来的你
 *
 * CEO \CTO\ CFO  等都可以使用同一个接口， 因为他们的秘书事项内容差不多
 *
 * 如果是现在是干家务事， 需要“保姆”代理类呢？ 又要创建保姆代理？
 * ...
 */
public class CEO implements  ISEO {
    // 会见
    @Override
    public void meeting()
    {
        System.out.println("和"+name+"谈成了34个亿的合作项目：给河道贴瓷砖！");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    // ..出差
    // ..开会

    // 客户名字
    private String name="";

    public CEO(String name) {
        this.name = name;
    }

}
